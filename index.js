/*
CRUD OPERATIONS

1. CREATE
		-add/insert new documents to a collection.

		syntax:

		db.collection.insertOne(document)
		db.collection.insertMany() 

		Example:
		To insert document:

		
2. READ
		-retrieve the data
		-find


3. UPDATE
		-modify existing documents in a collection.

		syntax:
			db.collection.updateOne(filter, update)
			db.collection.updateMany()
			db.collection.replaceOne()

4. DELETE
		
		syntax:
			db.collection.deleteOne() 
			db.collection.deleteMany()

*/
db.users.insertOne(
	{
		"firstName":"Jane",
		"lastName": "Doe",
		"age":21,
		"contact": {
			"phone": "87654321",
			"email": "jd@mail.com"
		},
		"courses": ["CSS", "Javascript","Phyton"],
		"department": "none"
	}
)



db.users.insertMany(
		[	
			{
				"firstName": "Stephen",
				"lastName": "Hawking",
				"age": 76,
				"contact": {
					"phone": "87654321",
					"email": "stephenhawking@gmail.com"
				},
				"courses": ["Python", "React", "PHP"],
				"department": "none"
			}, 
			{
				"firstName": "Neil",
				"lastName": "Armstrong",
				"age": 82,
				"contact": {
					"phone": "87654321",
					"email": "neilarmstrong@gmail.com"
				},
				"courses": ["React", "Laravel", "Sass"],
				"department": "none"
			}
		]
		)
		
db.users.find()

// db.users.find({"lastName": "Armstrong"})

db.users.insertOne(
			{
				"firstName": "Test",
				"lastName": "Test",
				"age": 0,
				"contact": {
					"phone": "0",
					"email": "Test"
				},
				"courses": [],
				"department": "Test"
			})

// filter and update
db.users.updateOne(
		{
			"firstName": "Test"
		},
		{	
			$set: {
				"firstName": "Bill",
				"lastName": "Gates",
				"age": 65,
				"contact": {
					"phone": "12345678",
					"email": "bg@mail.com"
				},
				"courses": ["PHP", "Laravel", "HTML"],
				"department": "Operations",
				"status": "active"
				}
		}
	)

db.users.updateOne(
		{
			"_id" : ObjectId("621f2c2463088f19ef61917e"),
		},
		{	
			$set: {
					"isAdmin":true
				}
		}
	)


db.users.updateOne(
		{
			"_id" : ObjectId("621f2c2463088f19ef61917e"),
		},
		{	
			$unset: {
					"isAdmin":true
				}
		}
	)

db.users.updateMany(
		{
			"department": "none"
		},
		{	
			$set: {
				"department": "HR"
			}
		}
	)


db.users.updateMany(
		{
			"department": "HR"
		},
		{	
			$set: {
				"status": "active"
			}
		}
	)


// first document only will be updated
db.users.updateOne(
		{
			"department": "HR"
		},
		{	
			$set: {
				"isAdmin":false
			}
		}
	)

// updateOne vs replaceOne
db.users.insertOne(
			{
				"firstName": "Test",
				"lastName": "Test",
				"age": 0,
				"contact": {
					"phone": "0",
					"email": "Test"
				},
				"courses": [],
				"department": "Test"
			})

db.users.updateOne(
		{
			"firstName": "Test"
		},
		{	
			$set: {
				"firstName": "Kristin"
			}
		}
	)

db.users.replaceOne(
	{"lastName":"Test"},
	{"lastName":"Ramos"}
	)


// DELETE METHOD

db.users.deleteOne(
	{"lastName":"Ramos"}
)
 
db.users.insertMany(
		[
			{"course":"JS",
			"price":1000},
			{"course":"JAVA",
			"price":3000},
			{"course":"PHP",
			"price":5000}
		]
)
		
db.users.deleteMany(
	{ price: { $gte: 3000 } }
)

db.users.insertMany(
		[
			{"course":"JAVA",
			"price":3000},
			{"course":"PHP",
			"price":5000}
		]
)

// db.users.deleteMany(
// 	{ 
// 		"_id":ObjectId("622013a80a05501cc1e91949"),
// 		"_id":ObjectId("6220158c0a05501cc1e9194c"),
// 		"_id":ObjectId("6220158c0a05501cc1e9194d"),
// 	}
// )


db.users.deleteMany(
	{ 
		"_id": { $in: 
			[	ObjectId("622013a80a05501cc1e91949"),
				ObjectId("6220158c0a05501cc1e9194c")
			]
			}
	}
)







